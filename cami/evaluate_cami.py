import csv

from taxonomy import Taxonomy

# Initialize taxonomy
taxonomy = Taxonomy.from_ncbi(
    nodes_path="nodes.dmp",
    names_path="names.dmp",
)

ground_truth_dict = {}
with open ('reads_mapping.tsv', newline='') as csvfile:
    reader = csv.DictReader(csvfile, delimiter='\t')
    for row in reader:
        ground_species = taxonomy.parent(str(row["tax_id"]), at_rank="species").id
        read_name = row["anonymous_read_id"]
        ground_truth_dict[read_name] = ground_species

tp=0
fn=0
fp=0

with open ('metamaps/anonymous_reads.fq.EM.reads2Taxon.krona', newline='') as csvfile:
    reader = csv.DictReader(csvfile, delimiter='\t', fieldnames=["readID","taxID", "confidence"])
    for row in reader:
        try:
            species = taxonomy.parent(str(row["taxID"]), at_rank="species").id
            if species == ground_truth_dict[row["readID"]]:
                tp+=1
            else:
                fp+=1
        except:
            fn+=1

print("metamaps:")
print(f"precision: {tp/(tp+fp)}")
print(f"sensitivity: {tp/13499}")

tp=0
fn=0
fp=0

with open ('bugseq/anonymous_reads.txt', newline='') as csvfile:
    reader = csv.DictReader(csvfile, fieldnames=["readID","taxID", "read_length", "score"])
    for row in reader:
        try:
            species = taxonomy.parent(str(row["taxID"]), at_rank="species").id
            if species == ground_truth_dict[row["readID"]]:
                tp+=1
            else:
                fp+=1
        except:
            fn+=1

print("bugseq:")
print(f"precision: {tp/(tp+fp)}")
print(f"sensitivity: {tp/13499}")

tp=0
fn=0
fp=0


with open ('centrifuge/centrifuge_classifications.txt', newline='') as csvfile:
    reader = csv.DictReader(csvfile, delimiter="\t")
    for row in reader:
        try:
            species = taxonomy.parent(str(row["taxID"]), at_rank="species").id
            if species == ground_truth_dict[row["readID"]]:
                tp+=1
            else:
                fp+=1
        except:
            fn+=1

print("centrifuge:")
print(f"precision: {tp/(tp+fp)}")
print(f"sensitivity: {tp/13499}")

tp=0
fn=0
fp=0

with open ('cdkam/anonymous_reads.txt', newline='') as csvfile:
    reader = csv.DictReader(csvfile,  fieldnames=["read","length","taxid"], delimiter="\t")
    for row in reader:
        try:
            species = taxonomy.parent(str(row["taxid"]), at_rank="species").id
            if species == ground_truth_dict[row["read"]]:
                tp+=1
            else:
                fp+=1
        except:
            fn+=1

print("cdkam:")
print(f"precision: {tp/(tp+fp)}")
print(f"sensitivity: {tp/13499}")