"""
Instructions
1) Run ncbi = NCBITaxa() to ensure that a local copy of NCBI taxononomy 
files are saved in home directory (in the format ete toolkit needs). 
2) Save classification file 'Zymo-GridION-LOG-BB-SN-flipflop.fq.tsv.rcf.data.tsv'
and 'expected_microbes.csv' in home directory. 
3) Run <python precision.py>

4) Results will look like below: 

Precision at superkingdom level is 99.9673 %
Expected superkingdom: ['Bacteria']



Precision at phylum level is 99.9653 %
Expected phylum: ['Proteobacteria' 'Firmicutes']



Precision at class level is 99.9436 %
Expected class: ['Gammaproteobacteria' 'Bacilli']



Precision at order level is 99.9409 %
Expected order: ['Pseudomonadales' 'Enterobacterales' 'Bacillales' 'Lactobacillales']



Precision at family level is 99.9382 %
Expected family: ['Pseudomonadaceae' 'Enterobacteriaceae' 'Staphylococcaceae' 'Bacillaceae'
 'Listeriaceae' 'Lactobacillaceae' 'Enterococcaceae']



Precision at genus level is 99.9379 %
Expected genus: ['Pseudomonas' 'Escherichia' 'Salmonella' 'Staphylococcus' 'Bacillus'
 'Listeria' 'Lactobacillus' 'Enterococcus']



Precision at species_group level is 99.9509 %
Expected species_group: ['Pseudomonas aeruginosa group' 'Bacillus subtilis group']



Precision at species level is 99.9191 %
Expected species: ['Pseudomonas aeruginosa' 'Escherichia coli' 'Salmonella enterica'
 'Staphylococcus aureus' 'Bacillus subtilis' 'Listeria monocytogenes'
 'Lactobacillus fermentum' 'Enterococcus faecalis']

"""

# Imports
import pandas as pd
import numpy as np
from ete3 import NCBITaxa
import os 

pd.options.display.max_rows = 999




# Define class instance

ncbi = NCBITaxa()


        
# Selectively comment out below for analyses
"""
seq_file='metaMaps_zymoEven_miniSeq.EM.WIMP.tsv'
seq_file='zymoLog_metamaps_miniseq.EM.WIMP.tsv'
seq_file='zymoLog_metaMaps_refseq.EM.WIMP.tsv'
"""


# For metamaps, use the altered below code 

# Define list needed later
ordered_levels = ['superkingdom', 'phylum', 'order', 'family',
                  'genus', 'species']


data = pd.read_csv(seq_file, sep='\t',
                usecols=['Stats', 'count', 'Rank', 'Name'])
data.columns=['taxid', 'count', 'rank', 'name']
expected=pd.read_csv('expected_microbes.csv', index_col=0) #index col just sets row labels to the content of the first column

# Get dataframes subsetted at different taxonomic levels from 'ordered_levels'.
# Also get (sum of counts of expected microbes)*100/(sum of all counts) at 
# different taxonomic levels from 'ordered_levels'.
new_df={}
new_df2={}
precision={}
for val in ordered_levels:
    df1=data[data['rank']==val] ##she separates printing by the rank... i.e first does superkingdom, then does phylum, etc.
    df2=df1[df1['taxid'].isin(expected[val])] ##she basically goes through the ordered levels, so superkingdom -> phylum -> class -> order -> family -> genus -> species_group -> species
    #and extracts a second dataframe with only those values, i.e df2 has Bacteria with the count and taxid, and rank
    #print(df2)
    new_df2[val]=df2['name'].values ##store the values of df2 into the new_df2 dictionary with key = the ordered level, value = an array of the names, i.e bacteria for superingdom
    ## at this point, new_df2 is a dictionary with key = the ordered level, value = an array of the names
    #print(new_df2)
    new_df[val]=df1 #associate ordered list value with that of df1  
    #print(new_df)

    precision[val]=(df2['count'].sum()/df1['count'].sum())*100


    #print(df1['count'].sum())
    #print(df2['count'].sum())



# Display results
for val in ordered_levels:
    #print(new_df[val])
	#print(new_df[val][new_df[val].taxid.isin(expected[val])])
    print('\nPrecision at {} level is {} %'.format(val,round(precision[val],4)))
    print('Expected {}: {}'.format(val,new_df2[val]))






